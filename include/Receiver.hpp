#ifndef EL_RECEIVER_HPP
#define EL_RECEIVER_HPP

#include <iostream>
#include <tuple>
#include <functional>
#include <unordered_map>
#include <unordered_set>
#include "Event.hpp"
#include "Channel.hpp"
#include "ReceiverUtils.hpp"

namespace el {
  
  template <typename T>
  class Receiver {

    public:
      Receiver() = default;

      ~Receiver() {
        for (auto& [channel, eventIDs] : subscribedChannels) {
          for (auto eventID : eventIDs) {
            channel->unsubscribe(eventID, *this);
          }
        }
      }

      const ReceiverID receiverID() {
        return ReceiverID((i64)(&Receiver::dummy), _receiverID);
      }

      template <typename E>
      void subscribe(el::Channel& channel) {
        channel.subscribe<E>(*static_cast<T*>(this));

        auto eventID = Event<E>::id();
        el::Channel* channelPtr = std::addressof(channel);
        subscribedChannels[channelPtr].insert(eventID);
      }

      template <typename E>
      void unsubscribe(el::Channel& channel) {
        channel.unsubscribe<E>(*static_cast<T*>(this));

        auto eventID = Event<E>::id();
        el::Channel* channelPtr = std::addressof(channel);
        subscribedChannels[channelPtr].erase(eventID);

        // perhaps delete entry from map if set size is zero?
      }
    
    private:
      std::unordered_map<el::Channel*, 
        std::unordered_set<EventID>> subscribedChannels;

      const i64 _receiverID = idCount++;
      static i64 idCount; 

      static void dummy() {}

      //add vector of pointers to event managers that the reciever is subscribed to 
  };

  template <typename T>
  i64 Receiver<T>::idCount = 0;

}

#endif

#ifndef EL_EVENT_HPP
#define EL_EVENT_HPP

#include <string>

namespace el {

  using EventID = long long;

  template <typename T>
  class Event {
    public:
      static constexpr inline EventID id() {
        return reinterpret_cast<EventID>(&Event::dummy);
      };

    private:
      static void dummy() {};
  };
  
  class TestEvent {
    public:
      TestEvent() = default;
      // TODO: fix issue where lack of explicit keyword means that receivers
      // can take raw string events with no string receive but a TestEvent
      // receive instead
      TestEvent(std::string message) {
        this->message = message;
      }

      std::string message; 
      friend std::ostream& operator<<(std::ostream& os, const TestEvent& t) {
        os << t.message;
        return os;
      }
  };
}

#endif 

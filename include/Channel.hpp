#ifndef EL_CHANNEL_HPP
#define EL_CHANNEL_HPP

#include <unordered_map>
#include <vector>
#include <iostream>
#include <functional>
#include <tuple>
#include "ReceiverUtils.hpp"

namespace el {

  // Function wrapper for subscriber receive methods
  struct ReceiverFunc {
    using SubscriberPtrType = void*;
    using SendEventPtrType = void(*)(void*, void*);

    SubscriberPtrType subscriberPtr;
    SendEventPtrType sendEventPtr;

    ReceiverFunc() = default;

    ReceiverFunc(SubscriberPtrType subscriberPtr, 
                 SendEventPtrType sendEventPtr)
    : subscriberPtr(subscriberPtr),
      sendEventPtr(sendEventPtr)
    {}

    void operator()(void* eventPtr) const {
      sendEventPtr(subscriberPtr, eventPtr);
    }
  };

  // need to make factory method to pass template param E
  template <typename E, typename T>
  ReceiverFunc makeReceiverFunc(T& subscriber) {
      return ReceiverFunc(std::addressof(subscriber), 
                          [](void* subscriberPtr, void* event) {
        static_cast<T*>(subscriberPtr)->receive(*static_cast<E*>(event));
      });
  }

  template <typename T, typename E>
  struct HasReceiveMethod {
    using True = char;
    using False = int;
    template<typename U, void(U::*)(const E&)> struct Check {};
    template<typename U> static True Test(Check<U, &U::receive>*);
    template<typename U> static False Test(...);
    static const bool value = sizeof(Test<T>(0)) == sizeof(True);
  };
  
  class Channel {
    public:
      Channel() = default; 

      template <typename E, typename T> //, enable if has function ...
      void subscribe(T& subscriber) {
        
        // easier testing
#ifdef DEBUG
        if (HasReceiveMethod<T, E>::value) {
          throw std::runtime_error("Subscriber needs matching receive method");
        }
#else
        static_assert(HasReceiveMethod<T, E>::value, 
                        "Subscriber needs matching receive method");
#endif

        ReceiverFunc receiver = makeReceiverFunc<E>(subscriber);

        auto eventID = Event<E>::id();
        auto receiverID = subscriber.receiverID();
        _receivers[eventID][receiverID] = receiver;
      }

      template <typename E, typename T>
      void unsubscribe(T& subscriber) {
        auto eventID = Event<E>::id();
        this->unsubscribe(eventID, subscriber);
      }

      template <typename T>
      void unsubscribe(EventID eventID, T& subscriber) {
        auto receiverID = subscriber.receiverID();
        _receivers[eventID].erase(receiverID);

        // perhaps delete event from map if map size is zero?
      }

      template <typename E>
      void emit(E& event) {
        auto eventID = Event<E>::id();
        for (const auto& [id, receiver] : _receivers[eventID]) {
          receiver(static_cast<void*>(std::addressof(event)));
        }
      }

      template <typename E, typename... Args>
      void emit(Args&&... args) {
         E event = E(std::forward<Args>(args)...);
         emit(event);
      }

      template <typename E>
      void emit(E&& event) {
        emit(event);
      }

      template <typename E>
      size_t size() {
        auto eventID = Event<E>::id();
        return _receivers[eventID].size();
      }

    private:
      std::unordered_map<EventID, std::unordered_map<ReceiverID, ReceiverFunc, ReceiverIDHash>> _receivers;
  };
}

#endif

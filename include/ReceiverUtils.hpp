#ifndef EL_RECEIVERUTILS_HPP
#define EL_RECEIVERUTILS_HPP

#include <tuple>
#include <functional>

namespace el {

  using i64 = long long;
  using ReceiverID = std::pair<i64, i64>;

  struct ReceiverIDHash {
    std::size_t operator()(const ReceiverID& r) const {
      std::hash<i64> hasher;
      return hasher(r.first) * 409806659 ^ hasher(r.second);
    }
  };

}


#endif

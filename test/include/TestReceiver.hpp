#ifndef EL_TESTRECEIVER_HPP
#define EL_TESTRECEIVER_HPP
#include <vector>
#include <tuple>
#include "Receiver.hpp"

namespace el {

  template<typename... Ts>
  class TestReceiver : public el::Receiver<TestReceiver<Ts...>> {
    public:
      std::tuple<std::vector<Ts>...> messageQueue;

      template <typename T>
      std::vector<T>& getMessageQueue() {
        return std::get<std::vector<T>>(messageQueue);
      }

      template <typename T>
      void receive(const T& message) {
        auto& selectedMessageQueue = this->getMessageQueue<T>();
        selectedMessageQueue.push_back(message);
      }
  };

}

#endif

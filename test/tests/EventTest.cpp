#include "catch.hpp"
#include <vector>
#include "Event.hpp"

TEST_CASE("Event IDs are unique", "[Event]") {

  SECTION("Built-in types get unique IDs") {
    std::vector<el::EventID> eventIDs{
      el::Event<int>::id(), 
      el::Event<char>::id(),
      el::Event<unsigned int>::id(),
      el::Event<std::vector<int>>::id(),
      el::Event<std::string>::id()
    };

    std::sort(eventIDs.begin(), eventIDs.end());
    auto it = std::adjacent_find(eventIDs.begin(), eventIDs.end());

    REQUIRE(it == eventIDs.end());    
  }

}

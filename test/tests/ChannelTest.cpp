#include "catch.hpp"
#include <vector>
#include <string>
#include "Receiver.hpp"
#include "TestReceiver.hpp"
#include "Channel.hpp"

TEST_CASE("Receiver subscribes to messages correctly", "[Receiver, Channel]") {

  el::Channel channel;

  SECTION("Receiver subscribes to built-in types correctly") {
    using DefaultReceiver = el::TestReceiver<int, long>;    

    DefaultReceiver receiverA;
    DefaultReceiver receiverB;

    receiverA.subscribe<int>(channel);
    receiverB.subscribe<long>(channel);

    int intMessage = 20;
    long longMessage = 40;

    channel.emit(intMessage);
    channel.emit(longMessage);

    REQUIRE(receiverA.getMessageQueue<int>().size() > 0);
    REQUIRE(receiverB.getMessageQueue<long>().size() > 0);

    REQUIRE(channel.size<int>() == 1);
    REQUIRE(channel.size<long>() == 1);

    REQUIRE(receiverA.getMessageQueue<long>().size() == 0);
    REQUIRE(receiverB.getMessageQueue<int>().size() == 0);

    REQUIRE(receiverA.getMessageQueue<int>().front() == 20);
    REQUIRE(receiverB.getMessageQueue<long>().front() == 40);
  }

  SECTION("Receiver subscribes to complex types correctly") {
    using ComplexReceiver = el::TestReceiver<std::string, std::vector<char>>;
    
    ComplexReceiver receiverA;
    ComplexReceiver receiverB;

    receiverA.subscribe<std::string>(channel);
    receiverB.subscribe<std::vector<char>>(channel);

    std::string stringMessage = "hello";
    std::vector<char> vectCharMessage = {'d'};

    channel.emit(stringMessage);
    channel.emit(vectCharMessage);

    REQUIRE(receiverA.getMessageQueue<std::string>().size() > 0);
    REQUIRE(receiverB.getMessageQueue<std::vector<char>>().size() > 0);

    REQUIRE(channel.size<std::string>() == 1);
    REQUIRE(channel.size<std::vector<char>>() == 1);

    REQUIRE(receiverA.getMessageQueue<std::vector<char>>().size() == 0);
    REQUIRE(receiverB.getMessageQueue<std::string>().size() == 0);

    REQUIRE(receiverA.getMessageQueue<std::string>().front() == "hello");
    REQUIRE(receiverB.getMessageQueue<std::vector<char>>().front()[0] == 'd');
  }

  SECTION("Receiver subscribes to user defined types correctly") {
    class TestStructA {
      public:
        explicit TestStructA(int data) : data(data) {}
        int data;
    };

    class TestStructB {
      public:
        explicit TestStructB(std::string data) : data(data) {}
        std::string data;
    };

    using CustomReceiver = el::TestReceiver<TestStructA, TestStructB>;

    CustomReceiver receiverA;
    CustomReceiver receiverB;

    receiverA.subscribe<TestStructA>(channel);
    receiverB.subscribe<TestStructB>(channel);

    auto messageA = TestStructA(10);
    auto messageB = TestStructB("test");

    channel.emit(messageA);
    channel.emit(messageB);

    REQUIRE(receiverA.getMessageQueue<TestStructA>().size() > 0);
    REQUIRE(receiverB.getMessageQueue<TestStructB>().size() > 0);

    REQUIRE(channel.size<TestStructA>() == 1);
    REQUIRE(channel.size<TestStructB>() == 1);

    REQUIRE(receiverA.getMessageQueue<TestStructB>().size() == 0);
    REQUIRE(receiverB.getMessageQueue<TestStructA>().size() == 0);

    REQUIRE(receiverA.getMessageQueue<TestStructA>().front().data == 10);
    REQUIRE(receiverB.getMessageQueue<TestStructB>().front().data == "test");
  }

}

TEST_CASE("Receiver receives messages in correct order", "[Receiver, Channel]") {

  el::Channel channel;

  using IntReceiver = el::TestReceiver<int>;
  IntReceiver receiver;
  receiver.subscribe<int>(channel);

  channel.emit(17);
  channel.emit(24);
  channel.emit(2);

  REQUIRE(receiver.getMessageQueue<int>().size() == 3);
  REQUIRE(receiver.getMessageQueue<int>()[0] == 17);
  REQUIRE(receiver.getMessageQueue<int>()[1] == 24);
  REQUIRE(receiver.getMessageQueue<int>()[2] == 2);
}

TEST_CASE("Receiver can only subscribe once for each type", "[Receiver, Channel]") {
  
  el::Channel channel;

  using IntReceiver = el::TestReceiver<int>;
  IntReceiver receiver;
  receiver.subscribe<int>(channel);
  receiver.subscribe<int>(channel);

  channel.emit(12);

  REQUIRE(receiver.getMessageQueue<int>().size() == 1);
  REQUIRE(receiver.getMessageQueue<int>()[0] == 12);
}

TEST_CASE("Receiver receives from channels in correct order", "[Receiver, Channel]") {

  el::Channel channelA;
  el::Channel channelB;

  using IntReceiver = el::TestReceiver<int>;
  IntReceiver receiver;
  receiver.subscribe<int>(channelA);
  receiver.subscribe<int>(channelB);

  channelA.emit(4);
  channelB.emit(28);

  REQUIRE(receiver.getMessageQueue<int>().size() == 2);
  REQUIRE(receiver.getMessageQueue<int>()[0] == 4);
  REQUIRE(receiver.getMessageQueue<int>()[1] == 28);
}

TEST_CASE("Receiver unsubscribes correctly", "[Receiver, Channel]") {
  
  el::Channel channel;

  using IntReceiver = el::TestReceiver<int>;
  IntReceiver receiver;

  REQUIRE(channel.size<int>() == 0);

  receiver.subscribe<int>(channel);

  REQUIRE(channel.size<int>() == 1);

  receiver.unsubscribe<int>(channel);

  REQUIRE(channel.size<int>() == 0);
}

TEST_CASE("Receivers unsubscribe automatically on destruction", "[Receiver, Channel]") {
  
  el::Channel channel;

  using IntReceiver = el::TestReceiver<int>;

  REQUIRE(channel.size<int>() == 0); 

  {
    IntReceiver receiver;
    receiver.subscribe<int>(channel);

    REQUIRE(channel.size<int>() == 1);
  }
  
  REQUIRE(channel.size<int>() == 0);
}
